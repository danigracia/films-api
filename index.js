const express = require("express")
require('dotenv').config();
const app = express()

//Conectar con la DB
require("./config/db")

//Habilitar express.json
app.use( express.json({extended: true}) )

//Routes
app.use("/api/films/", require("./routes/films"))
app.use("/api/auth/", require("./routes/auth"))

//Server
const PORT = process.env.PORT || 4000
app.listen(PORT, () => {
    console.log(`Servidor listo en http://localhost:${PORT}`)
})