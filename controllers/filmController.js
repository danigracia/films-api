const { Film } = require("../config/db")

exports.getFilms = async(req, res) => {
    const films = await Film.findAll()
    res.json(films)
}

exports.createFilm = async(req, res) => {
    try {
        const film = await Film.create(req.body)
        res.json(film)
    } catch (error) {
        console.log(error)
        res.status(500).json({error: "Internal server error"})
    }
}

exports.updateFilm = async(req, res) => {
    try {
        await Film.update(req.body, {
            where: { id: req.params.id }
        })
        res.json({ status: "success" })
    } catch (error) {
        console.log(error)
        res.status(500).json({error: "Internal server error"})
    }

}

exports.deleteFilm = async(req, res) => {
    try {
        await Film.destroy({
            where: { id: req.params.id }
        })
        res.json({ status: "success" })
    } catch (error) {
        console.log(error)
        res.status(500).json({error: "Internal server error"})
    }
}

exports.getFilmById = async(req, res) => {
    try {
        const film = await Film.findOne({ where: { id: req.params.id } });
        res.json(film)
    } catch (error) {
        console.log(error)
        res.status(500).json({error: "Internal server error"})
    }
}