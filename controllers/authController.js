const { User } = require("../config/db")
require('dotenv').config();
const bcrypt = require("bcryptjs")
const { validationResult } = require("express-validator")
const moment = require("moment")
const jwt = require("jwt-simple")

exports.register = async (req, res) => {

    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(422).json({errors: errors.array()})
    }

    try {
        req.body.password = bcrypt.hashSync(req.body.password, 10)
        const user = await User.create(req.body)
        res.json(user)
    } catch (error) {
        console.log(error)
        res.status(500).json({error: "Internal server error"})
    }

}

exports.login = async (req, res) => {
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(422).json({errors: errors.array()})
    }

    const user = await User.findOne({ where: { username: req.body.username } })

    //If the user does not exist
    if(!user) return res.json({error: "El usuario o la contraseña no son validos"})

    //Compare passwords
    const passwordCorrect = bcrypt.compareSync(req.body.password, user.password)

    //If the password is incorrect
    if(!passwordCorrect) return res.json({error: "El usuario o la contraseña no son validos"})

    //LOGIN SUCCESS
    res.json({token: createToken(user)})

}

const createToken = (user) => {
    const payload = {
        userId: user.id,
        createdAt: moment().unix(),
        expiredAt: moment().add(2, "hours").unix()
    }

    return jwt.encode(payload, process.env.SECRET_KEY)
}