require('dotenv').config();
const Sequelize = require("sequelize")

const FilmModel = require("../models/Film")
const UserModel = require("../models/User")

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: process.env.DB_TYPE
})

const Film = FilmModel(sequelize, Sequelize)
const User = UserModel(sequelize, Sequelize)

sequelize.sync({ force: false })
    .then(() => console.log("Migrations ran successfully"))

module.exports = {
    Film,
    User
}