// ROUTES FILMS
const router = require("express").Router()
const filmController = require("../controllers/filmController")
const auth = require("../middlewares/auth")

//Get all
router.get("/", filmController.getFilms)

//Get 1 by Id
router.get("/:id", filmController.getFilmById)

//Create
router.post("/", auth, filmController.createFilm)

//Update
router.put("/:id", auth, filmController.updateFilm)

//Delete
router.delete("/:id", auth, filmController.deleteFilm)

module.exports = router