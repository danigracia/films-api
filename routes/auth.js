// ROUTES USERS
const router = require("express").Router()
const { check } = require("express-validator")
const authController = require("../controllers/authController")

router.post("/register", 
[
    check("username", "The username is required").not().isEmpty(),
    check("password", "The password is required").not().isEmpty(),
    check("email", "The email is required").not().isEmpty(),
    check("email", "The email is invalid").isEmail()
]
,authController.register)

router.post("/login",
[
    check("username", "The username is required").not().isEmpty(),
    check("password", "The password is required").not().isEmpty(),
]
,authController.login)

module.exports = router