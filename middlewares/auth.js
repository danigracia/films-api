const jwt = require("jwt-simple")
const moment = require("moment")

const auth = (req, res, next) => {

    //Check if the request contains a token
    if(!req.headers["user-token"]){
        return res.json({ error: "You need a valid token" })
    }

    const token = req.headers["user-token"]
    let payload = {}

    try {
        payload = jwt.decode(token, process.env.SECRET_KEY)
    } catch (error) {
        return res.json({error: "The token is invalid"})
    }
    
    //Check if the token is expired
    if(payload.expiredAt < moment().unix()){
        return res.json({error: "The token has expired"})
    }

    next()
}

module.exports = auth